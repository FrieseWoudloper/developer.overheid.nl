module gitlab.com/commonground/developer.overheid.nl

require (
	github.com/RoaringBitmap/roaring v0.4.17 // indirect
	github.com/Smerity/govarint v0.0.0-20150407073650-7265e41f48f1 // indirect
	github.com/blevesearch/bleve v0.7.0
	github.com/blevesearch/blevex v0.0.0-20180227211930-4b158bb555a3 // indirect
	github.com/blevesearch/go-porterstemmer v1.0.2 // indirect
	github.com/blevesearch/segment v0.0.0-20160915185041-762005e7a34f // indirect
	github.com/blevesearch/snowballstem v0.0.0-20180110192139-26b06a2c243d // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/couchbase/vellum v0.0.0-20190403125519-462e86d8716b // indirect
	github.com/cznic/b v0.0.0-20181122101859-a26611c4d92d // indirect
	github.com/cznic/mathutil v0.0.0-20181122101859-297441e03548 // indirect
	github.com/cznic/strutil v0.0.0-20181122101858-275e90344537 // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jessevdk/go-flags v1.4.0
	github.com/jmhodges/levigo v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/remyoudompheng/bigfft v0.0.0-20190512093250-babf20351dd7 // indirect
	github.com/steveyen/gtreap v0.0.0-20150807155958-0abe01ef9be2 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tecbot/gorocksdb v0.0.0-20190519124805-025c3cf4ffb4 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	golang.org/x/net v0.0.0-20190520211455-018c4d40a106 // indirect
	golang.org/x/sys v0.0.0-20190516115306-61b9204099cb // indirect
)
