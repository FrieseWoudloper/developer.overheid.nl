import styled from 'styled-components'

export const StyledAPIFilters = styled.div`
  padding: 20px;
  background: #f0f2f7;
  border-radius: 3px;
`
