import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  max-width: 1240px;
  margin: 0 40px;
  flex-wrap: wrap;
`
